/**
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020 Val Vachaev <http://www.vachaev.net>

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef DHTWORKER_H
#define DHTWORKER_H

#include <QObject>
#include <QPointer>
#include <QThread>
#include <QTimer>
#include "RpiGpioBasic.h"

/**
 *
 * The DhtWorker class provides reading of DHT22 sensor output
 * in the separated thread
 */

class DhtWorker : public QObject
{
    Q_OBJECT

public:

    DhtWorker(int gpioNum = 0, int milliseconds = 0, QObject *parent = nullptr);
    ~DhtWorker();

    inline void stop()  { m_isFinish = true; }

public slots:
    void run();
    void slotFinish();

protected slots:
   void readDht();

signals:
    void sigResult(double t, double h);
    void finished();

private:
    void busy_wait_milliseconds(uint32_t millis);
    void sleep_milliseconds(uint32_t millis);

private:
    int m_result;
    uint32_t m_pulseCounts[41*2];
    QString szTemperature;
    QString szHumidity;
    QPointer<RpiGpioBasic> m_gpio;
    QTimer* m_timer;
    int m_milliseconds;
    bool m_isFinish;
};


#endif // DHTWORKER_H
