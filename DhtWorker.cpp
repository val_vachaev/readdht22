/**
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020 Val Vachaev <http://www.vachaev.net>

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

extern "C"
{
#include <errno.h>
#include <sched.h>
#include <string.h>
#include <sys/time.h>
#include <time.h>
}

#include <QDebug>

#include "DhtWorker.h"

// Define errors and return values.
#define DHT_ERROR_TIMEOUT   -1
#define DHT_ERROR_CHECKSUM  -2
#define DHT_ERROR_ARGUMENT  -3
#define DHT_ERROR_GPIO      -4
#define DHT_SUCCESS         0

// This is the only processor specific magic value, the maximum amount of time to
// spin in a loop before bailing out and considering the read a timeout.  This should
// be a high value, but if you're running on a much faster platform than a Raspberry
// Pi or Beaglebone Black then it might need to be increased.
const int DHT_MAXCOUNT = 32000;

// Number of bit pulses to expect from the DHT.  Note that this is 41 because
// the first pulse is a constant 50 microsecond pulse, with 40 pulses to represent
// the data afterwards.
const int DHT_PULSES = 41;


DhtWorker::DhtWorker(int gpioNum, int milliseconds, QObject *parent) : QObject(parent),
    m_result(DHT_SUCCESS),
    m_pulseCounts(),
    szTemperature("00.0"),
    szHumidity("00.0"),
    m_gpio(new RpiGpioBasic(this)),
    m_timer(nullptr),
    m_milliseconds(milliseconds),
    m_isFinish(false)
{
    m_result = DHT_SUCCESS;
    for(int j=0; j< DHT_PULSES*2; j++)
    {
        m_pulseCounts[j] = 0;
    }

    if (gpioNum)
    {
        m_gpio->initPin(gpioNum);
    }
}

DhtWorker::~DhtWorker()
{
    if (m_timer)
    {
        m_timer->stop();
        delete m_timer;
    }

     emit finished();
}

void DhtWorker::slotFinish()
{
    qDebug() << "slotFinish...";
}

void DhtWorker::run()
{
     m_timer = new QTimer;
     m_timer->setInterval(m_milliseconds);
     connect(m_timer, SIGNAL(timeout()), this, SLOT(readDht()));
     m_timer->start();
}
/*
 *  timeout function
 *  reads the sensor in the separed thread
 */
void DhtWorker::readDht()
{
    if (m_isFinish)
    {
        if (m_timer)
        {
            m_timer->stop();
            delete m_timer;
            m_timer = nullptr;
        }

        emit finished();
        return;
    }

    // Store the count that each DHT bit pulse is low and high.
    // Make sure array is initialized to start at zero.
    m_result = DHT_SUCCESS;
    for(int j=0; j< DHT_PULSES*2; j++)
    {
        m_pulseCounts[j] = 0;
    }

    // Set pin to output.
    m_gpio->setDirection(RpiGpioBasic::DirectionType::Out);

    // Bump up process priority and change scheduler to try to try to make process more 'real time'.
    QThread::currentThread()->setPriority(QThread::HighestPriority);

    // Set pin high for ~500 milliseconds.
     m_gpio->setPinStatus(RpiGpioBasic::PinStatus::High);
    sleep_milliseconds(500);

    // The next calls are timing critical and care should be taken
    // to ensure no unnecssary work is done below.

    // Set pin low for ~20 milliseconds.
     m_gpio->setPinStatus(RpiGpioBasic::PinStatus::Low);
    busy_wait_milliseconds(20);

    // Set pin at input.
     m_gpio->setDirection(RpiGpioBasic::DirectionType::In);

    // Need a very short delay before reading pins or else value is sometimes still low.
    for (volatile int i = 0; i < 50; ++i)
    {
        ;
    }

    // Wait for DHT to pull pin low.
    uint32_t count = 0;
    while ( m_gpio->getPinStatus() == 1)
    {
        if (++count >= DHT_MAXCOUNT)
        {
            // Timeout waiting for response.
            QThread::currentThread()->setPriority(QThread::InheritPriority);
            qDebug() << "Error  DHT_ERROR_TIMEOUT  1";
            m_result = DHT_ERROR_TIMEOUT;
            return;
        }
    }

    // Record pulse widths for the expected result bits.
    for (int i=0; i < DHT_PULSES*2; i+=2)
    {
        // Count how long pin is low and store in pulseCounts[i]
        while ( m_gpio->getPinStatus() == 0)
        {
            if (++m_pulseCounts[i] >= DHT_MAXCOUNT)
            {
                // Timeout waiting for response.
                QThread::currentThread()->setPriority(QThread::InheritPriority);
                qDebug() << "Error  DHT_ERROR_TIMEOUT 2  ";
                m_result = DHT_ERROR_TIMEOUT;
                return;
            }
        }
        // Count how long pin is high and store in pulseCounts[i+1]
        while ( m_gpio->getPinStatus() == 1)
        {
            if (++m_pulseCounts[i+1] >= DHT_MAXCOUNT)
            {
                // Timeout waiting for response.
                QThread::currentThread()->setPriority(QThread::InheritPriority);
                qDebug() << "Error  DHT_ERROR_TIMEOUT 3";
                m_result = DHT_ERROR_TIMEOUT;
                return;
            }
        }
    }

    // Done with timing critical code, now interpret the results.

    // Drop back to normal priority.
    QThread::currentThread()->setPriority(QThread::InheritPriority);

    // Compute the average low pulse width to use as a 50 microsecond reference threshold.
    // Ignore the first two readings because they are a constant 80 microsecond pulse.
    uint32_t threshold = 0;
    for (int i=2; i < DHT_PULSES*2; i+=2)
    {
        threshold += m_pulseCounts[i];
    }
    threshold /= DHT_PULSES-1;

    // Interpret each high pulse as a 0 or 1 by comparing it to the 50us reference.
    // If the count is less than 50us it must be a ~28us 0 pulse, and if it's higher
    // then it must be a ~70us 1 pulse.

    uint8_t data[5] = {0};
    for (int i=3; i < DHT_PULSES*2; i+=2)
    {
        int index = (i-3)/16;
        data[index] <<= 1;
        if (m_pulseCounts[i] >= threshold)
        {
            // One bit for long pulse.
            data[index] |= 1;
        }
        // Else zero bit for short pulse.
    }

    // Verify checksum of received data.
    if (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF))
    {
        // Calculate humidity and temp for DHT22 sensor.
        double humidity = (data[0] * 256 + data[1]) / 10.0;
        double temperature = ((data[2] & 0x7F) * 256 + data[3]) / 10.0;
        if (data[2] & 0x80)
        {
            temperature *= -1.0;
        }

        m_result = DHT_SUCCESS;
        emit sigResult(temperature, humidity);
    }
    else
    {
        qDebug() << "Error  DHT_ERROR_CHECKSUM";
        m_result = DHT_ERROR_CHECKSUM;
    }
}

void DhtWorker::busy_wait_milliseconds(uint32_t millis)
{
    // Set delay time period.
    struct timeval deltatime;
    deltatime.tv_sec = millis / 1000;
    deltatime.tv_usec = (millis % 1000) * 1000;
    struct timeval walltime;
    // Get current time and add delay to find end time.
    gettimeofday(&walltime, nullptr);
    struct timeval endtime;
    timeradd(&walltime, &deltatime, &endtime);
    // Tight loop to waste time (and CPU) until enough time as elapsed.
    while (timercmp(&walltime, &endtime, <))
    {
        gettimeofday(&walltime, nullptr);
    }
}

void DhtWorker::sleep_milliseconds(uint32_t millis)
{
    struct timespec sleep;
    sleep.tv_sec = millis / 1000;
    sleep.tv_nsec = (millis % 1000) * 1000000L;
    while (clock_nanosleep(CLOCK_MONOTONIC, 0, &sleep, &sleep) && errno == EINTR);
}
