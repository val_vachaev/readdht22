/**
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020 Val Vachaev <http://www.vachaev.net>

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef DHTMANAGER_H
#define DHTMANAGER_H

#include <QTimer>

/**
 *
 * The DhtManager class is exposing data to QML code and
 * connecting QML code with C++ class methods
 */

class DhtWorker;

class DhtManager : public QObject
{
    Q_OBJECT

public:
    DhtManager(QObject *parent = nullptr);
    virtual ~DhtManager();

    Q_PROPERTY(QString szTemperature READ getSzTemperature WRITE setSzTemperature NOTIFY szTemperatureChanged)
    Q_PROPERTY(QString szHumidity READ getSzHumidity  WRITE setSzHumidity NOTIFY szHumidityChanged)

    Q_INVOKABLE void setGpioPin(QString no)                 { initDHTMonitorPin(no); }
    Q_INVOKABLE void startPolling();

    inline QString getSzTemperature()                       { return szTemperature; }
    inline void setSzTemperature(const QString& str)        { szTemperature = str; }

    inline QString getSzHumidity()                          { return szHumidity; }
    inline void setSzHumidity(const QString& str)           { szHumidity = str; }

    void initDHTMonitorPin(QString gpioNum);
    void readSensor();

signals:
    void szTemperatureChanged();
    void szHumidityChanged();
    void sigFinish();

private slots:
    void slotDone(double t, double h);

private:
    // data
    int     m_gpioNum;
    QString szTemperature;
    QString szHumidity;
    bool    m_isPolling;
    DhtWorker* m_pWorker;
};

#endif // DHTMANAGER_H
