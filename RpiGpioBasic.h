/**
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020 Val Vachaev <http://www.vachaev.net>

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef RPIGPIOBASIC_H
#define RPIGPIOBASIC_H

#include <QObject>

/**
 *
 * The RpiGpioBasic Class is able to control the direction and state of all GPIO pins
 * on the RPI's 40-pin header using the 'mmaping into /dev/mem' approach. The advantage
 * of this approach is that GPIO toggling speeds of up to 21MHz can be achieved
 * whereas the toggling gpio speed using the sysfs interface is in the 100s of KHz.
 *
 * This class is not comprehensive as it does not enable PAD manipulation, pull-up control
 * or interrupts,but it is adequate for basic GPIO use
 *
 * */

class RpiGpioBasic : public QObject
{
    Q_OBJECT

public:
    // Enums
    enum EdgeType      { Falling, Rising, Both, None };
    enum DirectionType { In, Out, Unknown};
    enum PinStatus     { Low, High, NotDefined};

    RpiGpioBasic(QObject *parent= nullptr);
    RpiGpioBasic(int gpioNo, DirectionType direction, QObject *parent = nullptr);
    virtual ~RpiGpioBasic();

    void setPinStatus(int value);
    int  getPinStatus();
    void setDirection(const DirectionType & direction);

    void initPin(int pinNo, DirectionType direction = DirectionType::Unknown);
    bool initMonitorPin(int pinNo, EdgeType direction = EdgeType::Both, uint monitorPeriodSeconds = 0);
    int getAlt();
    int phisToGpio(int no);
    DirectionType getDirection();

private:
    bool initGpio();

private:

    int m_gpioNum;
    int m_status;
    DirectionType m_direction;

    EdgeType m_edge;  // Both by default
    uint m_monitoringPeriod;
    bool m_isMonitoringMode;
    bool m_stopRequest;

    volatile uint32_t* m_gpioAddr;
    // for rpi4 these parameters are different
    static const uint32_t GPIO_BASE = 0x3F200000;
    static const uint32_t GPIO_LEN  = 4096;

};

#endif // RPIGPIOBASIC_H
