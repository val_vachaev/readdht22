﻿import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.12
import lcdnumberplugin 1.0


ApplicationWindow {
    id: mainWin
    visible: true
    width: 640
    height: 480
    title: qsTr("Reading DHT22 sensor")

    property bool isSmallSize: false
    Material.foreground: Material.Blue

    Item {
        anchors.centerIn: parent
        width: 640
        height: 480

        Rectangle {
            id: setRect
            visible: true

            anchors.centerIn: parent
            width: parent.width * 0.5
            height: parent.height - 20

            color: "transparent"
            border.color: "steelblue"
            border.width: 2
            radius: 20

            Column {
                id: setCol
                anchors.fill: parent
                anchors.margins: (mainWin.isSmallSize)? 10 : 40
                spacing: (mainWin.isSmallSize)? 8 : 40

                property int boxWidth: setRect.width/2 + 80

                Label {
                    text: qsTr("Select GPIO Pin:")
                    font.pixelSize: 22
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                GroupBox {
                    id: modeBox

                    title: ""
                    width: setCol.boxWidth
                    anchors.horizontalCenter: parent.horizontalCenter

                    RowLayout {
                        id: rowLayout1
                        anchors.fill: parent
                        spacing: 40

                        ComboBox {
                            id: cbGPIO
                            width: parent.width * 4
                            Layout.fillWidth: true
                            currentIndex: 0
                            // available GPIOs
                            model:  [2, 3, 4, 5, 6, 7, 8, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]
                        }

                        Button {
                            id: modeChanged
                            text: qsTr("Set GPIO pin")
                            onClicked: {
                                DHTCtrl.setGpioPin(cbGPIO.currentText)
                            }
                        }
                    }
                }

                Button {
                    id: startId
                    text: qsTr("Start Polling")
                    anchors.horizontalCenter: parent.horizontalCenter

                    onClicked: {
                        DHTCtrl.startPolling()
                        startId.enabled = false
                        setRect.visible = false;
                        infoRect.visible = true;
                    }
                }

                Button {
                    text: qsTr("Exit")
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: startId.width
                    onClicked: Qt.quit()
                }
            }
        }

        Rectangle {
            id: infoRect
            visible: false

            anchors.centerIn: parent
            width: parent.width * 0.5
            height: parent.height - 20

            color: "transparent"
            border.color: "steelblue"
            border.width: 2
            radius: 20

            Column {
                id: tabloId
                anchors.fill: parent
                anchors.margins: 40
                spacing: 40

                DigLabel {
                    id: tempLabel
                    text: DHTCtrl.szTemperature
                    textLabel: qsTr("Temperature")
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                DigLabel {
                    id: humLabel
                    text: DHTCtrl.szHumidity
                    textLabel: qsTr("Humidity")
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Button {
                    text: qsTr("Exit")
                    anchors.horizontalCenter: parent.horizontalCenter
                    onClicked: Qt.quit()
                }
            }
        }

        Component.onCompleted: {
            console.log("Screen width: " + width + "  Screen height: " +  height);

            if (height < 500 || width < 500) {
                mainWin.isSmallSize = true
            }
        }
    }
}
