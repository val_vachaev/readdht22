/**
Licensed under the MIT License <http://opensource.org/licenses/MIT>.
SPDX-License-Identifier: MIT
Copyright (c) 2020 Val Vachaev <http://www.vachaev.net>

Permission is hereby  granted, free of charge, to any  person obtaining a copy
of this software and associated  documentation files (the "Software"), to deal
in the Software  without restriction, including without  limitation the rights
to  use, copy,  modify, merge,  publish, distribute,  sublicense, and/or  sell
copies  of  the Software,  and  to  permit persons  to  whom  the Software  is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE  IS PROVIDED "AS  IS", WITHOUT WARRANTY  OF ANY KIND,  EXPRESS OR
IMPLIED,  INCLUDING BUT  NOT  LIMITED TO  THE  WARRANTIES OF  MERCHANTABILITY,
FITNESS FOR  A PARTICULAR PURPOSE AND  NONINFRINGEMENT. IN NO EVENT  SHALL THE
AUTHORS  OR COPYRIGHT  HOLDERS  BE  LIABLE FOR  ANY  CLAIM,  DAMAGES OR  OTHER
LIABILITY, WHETHER IN AN ACTION OF  CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE  OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "RpiGpioBasic.h"

// GPIO Registers that can be accessed as a part of the class
static const uint32_t GPFSET0 = 7;
static const uint32_t GPFCLR0 = 10;
static const uint32_t GPFLEV0 = 13;

// gpioToGPFSEL:
//	Map a BCM_GPIO pin to it's Function Selection
//	control port. (GPFSEL 0-5)
//	Groups of 10 - 3 bits per Function - 30 bits per port

static uint8_t gpioToGPFSEL [] =
{
  0,0,0,0,0,0,0,0,0,0,
  1,1,1,1,1,1,1,1,1,1,
  2,2,2,2,2,2,2,2,2,2,
  3,3,3,3,3,3,3,3,3,3,
  4,4,4,4,4,4,4,4,4,4,
  5,5,5,5,5,5,5,5,5,5,
};

// gpioToShift
//	Define the shift up for the 3 bits per pin in each GPFSEL port

static uint8_t gpioToShift [] =
{
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
  0,3,6,9,12,15,18,21,24,27,
};


static int physToGpioR2[64] =
{
  -1,		// 0
  -1, -1,	// 1, 2
   2, -1,
   3, -1,
   4, 14,
  -1, 15,
  17, 18,
  27, -1,
  22, 23,
  -1, 24,
  10, -1,
   9, 25,
  11,  8,
  -1,  7,	// 25, 26
// B+
   0,  1,
   5, -1,
   6, 12,
  13, -1,
  19, 16,
  26, 20,
  -1, 21,
// the P5 connector on the Rev 2 boards:
  -1, -1,
  -1, -1,
  -1, -1,
  -1, -1,
  -1, -1,
  28, 29,
  30, 31,
  -1, -1,
  -1, -1,
  -1, -1,
  -1, -1,
} ;


/**
 *   C-tor
 **/

RpiGpioBasic::RpiGpioBasic(QObject *parent) :  QObject(parent)
  , m_gpioAddr(nullptr)
{
    initGpio();
}

RpiGpioBasic::RpiGpioBasic(int gpioNo, DirectionType direction, QObject *parent) : QObject(parent)
    , m_gpioAddr(nullptr)
{
    m_gpioNum = gpioNo;
    m_direction = direction;
}

RpiGpioBasic::~RpiGpioBasic()
{
    // unmap GPIO registers (physicalmemory)  from process memory
    if ((m_gpioAddr != MAP_FAILED) && munmap((void*)m_gpioAddr, GPIO_LEN) < 0)
    {
        qInfo("munmap (gpio) failed");
    }
}

int RpiGpioBasic::phisToGpio(int no)
{
    return physToGpioR2[no&63];
}

void RpiGpioBasic::initPin(int pinNo, RpiGpioBasic::DirectionType direction)
{
    m_gpioNum = pinNo;
    m_status = NotDefined;
    m_direction = direction;

    if (DirectionType::Unknown != direction)
    {
        setDirection(direction);
        if (getDirection() != direction)
        {
            qWarning("RpiGpioBasic::initPin SetDirection failed. File %d", m_gpioNum);
        }
    }
}
/**
 *    ! ToDo
 */
bool RpiGpioBasic::initMonitorPin(int pinNo, EdgeType edge, uint monitorPeriodSeconds)
{
    m_gpioNum = pinNo;
    m_direction = RpiGpioBasic::In;
    m_edge = edge;  // Both by default
    m_status = NotDefined;
    m_monitoringPeriod = monitorPeriodSeconds;
    m_isMonitoringMode = false;
    m_stopRequest = false;

    return true;
}

/**
 * setDirection() - sets the direction of a pin to
 * either input or output
 * Function select pin is a BCM2835 GPIO pin number NOT RPi pin number
 *      There are 6 control registers, each control the functions of a block
 *      of 10 pins.
 *      Each control register has 10 sets of 3 bits per GPIO pin:
 *
 *      000 = GPIO Pin X is an input                    IN
 *      001 = GPIO Pin X is an output                   OUT
 *      100 = GPIO Pin X takes alternate function 0     ALT0
 *      101 = GPIO Pin X takes alternate function 1     ALT1
 *      110 = GPIO Pin X takes alternate function 2     ALT2
 *      111 = GPIO Pin X takes alternate function 3     ALT3
 *      011 = GPIO Pin X takes alternate function 4     ALT4
 *      010 = GPIO Pin X takes alternate function 5     ALT5
 *
 * So the 3 bits for port X are:
      X / 10 + ((X % 10) * 3)
 **/

void RpiGpioBasic::setDirection(const DirectionType& direction)
{
    if (m_gpioAddr == MAP_FAILED)
    {
        qInfo("setDirection: m_gpioAddr == MAP_FAILED");
        return;
    }

    // first clear all bits for this gpio by declaring it an input
    // (and thus removing all ALT and output bits)

    uint32_t fSel  = gpioToGPFSEL[m_gpioNum] ;
    uint32_t shift = gpioToShift[m_gpioNum] ;

    *(m_gpioAddr + fSel) &= ~(7 << shift);

    if (direction != RpiGpioBasic::In)
    {
        // then set the mode
        *(m_gpioAddr + fSel) |= (static_cast<uint32_t>(direction) << shift);
    }
}

RpiGpioBasic::DirectionType RpiGpioBasic::getDirection()
{
    DirectionType direction = RpiGpioBasic::DirectionType::Unknown;

    if (m_gpioAddr == MAP_FAILED )
    {
        return direction;
    }

    uint32_t fSel    = gpioToGPFSEL[m_gpioNum] ;
    uint32_t shift   = gpioToShift[m_gpioNum] ;

    uint32_t reg = *(m_gpioAddr + fSel);
    reg = (reg >> shift) & 0x07;
    direction = (reg!=0)? RpiGpioBasic::Out : RpiGpioBasic::In;

    return direction;
}

/**
 *  reads the state of a GPIO pin and returns its value
 **/

int RpiGpioBasic::getPinStatus()
{
    int retVal = RpiGpioBasic::NotDefined;

    if (m_gpioAddr == MAP_FAILED)
    {
        return retVal;
    }

    if ((*(m_gpioAddr + GPFLEV0) & (1 << m_gpioNum)) != 0)
    {
        retVal = RpiGpioBasic::High;
    }
    else
    {
        retVal = RpiGpioBasic::Low;
    }

    return retVal;
}

/**
 * sets (to 1) or clears (to 0) the state of an
 * output GPIO. This function has no effect on input GPIOs.
 **/

void RpiGpioBasic::setPinStatus(int state)
{
    if (m_gpioAddr == MAP_FAILED)
    {
        return;
    }

    if(state == RpiGpioBasic::High)
        *(m_gpioAddr + GPFSET0) = (1 << m_gpioNum);
    else
        *(m_gpioAddr + GPFCLR0) = (1 << m_gpioNum);
}

/**
 * This function maps a block of physical memory into the memory of
 * the calling process. It enables a user space process to access
 * registers in physical memory directly without having to interact
 * with in kernel side code i.e. device drivers
 **/

bool RpiGpioBasic::initGpio()
{
    int fd;

    qInfo("RpiGpioBasic::initGpio");

    // handle to physical memory
    if ((fd = open("/dev/mem", O_RDWR | O_SYNC) ) < 0)
    {
        qCritical("Unable to open /dev/mem: %s\n", strerror(errno));
        return false;
    }

    // Map some of the pin mux register
    m_gpioAddr = static_cast<uint32_t *> (mmap(nullptr, GPIO_LEN, PROT_READ|PROT_WRITE, MAP_SHARED, fd, GPIO_BASE));
    close(fd);

    if (m_gpioAddr == MAP_FAILED)
    {
        qCritical("mmap failed: %s\n", strerror(errno));
        return false;
    }

    return true;
}

/**
 * returns the GPIO current mode
 */
int RpiGpioBasic::getAlt()
{
    uint32_t fSel, shift;

    if (m_gpioAddr == MAP_FAILED || m_gpioNum > 64)
    {
        return -1;
    }

    fSel  = gpioToGPFSEL[m_gpioNum];
    shift = gpioToShift[m_gpioNum];

    return (*(m_gpioAddr + fSel) >> shift) & 0x07;
}
